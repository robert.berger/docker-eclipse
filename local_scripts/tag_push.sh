set -x
docker images
docker tag reslocal/eclipse:latest reliableembeddedsystems/eclipse:latest
docker images
docker login --username reliableembeddedsystems
docker push reliableembeddedsystems/eclipse:latest
set +x
