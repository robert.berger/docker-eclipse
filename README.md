docker-eclipse
==============

Eclipse + Yocto plugins in a docker

run it with this script from here:

non-local_scripts/

└── docker_run.sh

default user:

username: genius

password: genius

https://hub.docker.com/r/reliableembeddedsystems/eclipse/

Tag Name	Compressed Size		Last Updated
====================================================
2018-07-02	856 MB			an hour ago

latest		856 MB			an hour ago
